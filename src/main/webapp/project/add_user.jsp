<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<!DOCTYPE html>
<%@ include file="../common/comm_css.jsp"%>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>新增用户</title>
		<meta name="keywords" content="">
		<meta name="description" content="">
	</head>

	<body class="gray-bg">
		<div class="wrapper wrapper-content animated">
			<div class="row">
				<div class="col-sm-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>新增用户</small></h5>
						</div>
						<div class="ibox-content">
							<form class="form-horizontal">
								<div class="form-group">
									<label class="col-sm-2 control-label">用户账户</label>

									<div class="col-sm-6">
										<input type="text" class="form-control" id="user_name">
									</div>
								</div>
								<div class="hr-line-dashed"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">用户密码</label>

									<div class="col-sm-6">
										<input type="text" class="form-control" id="password">
									</div>
								</div>
								<div class="hr-line-dashed"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">用户昵称</label>

									<div class="col-sm-6">
										<input type="text" class="form-control" id="real_name">
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="mar-t-20" style="text-align: center;">
				<button type="button" class="btn btn-w-m btn-info btn-lg" id="submit">提交</button>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<button type="button" class="btn btn-w-m btn-info btn-lg" id="exit">返回</button>
			</div>
		</div>
	</body>
</html>
<script type="text/javascript">
	
	$("#submit").click(function(){
		var xco = new XCO();
		
		var user_name = $("#user_name").val();
		if(user_name){
			xco.setStringValue("user_name",user_name);
		}else{
			layer.msg("请输入用户账户", {time: times, icon:no});
			return;
		}
		
		var password = $("#password").val();
		if(password){
			xco.setStringValue("password",password);
		}else{
			layer.msg("请输入用户密码", {time: times, icon:no});
			return;
		}
		
		var real_name = $("#real_name").val();
		if(real_name){
			xco.setStringValue("real_name",real_name);
		}else{
			layer.msg("请输入用户昵称", {time: times, icon:no});
			return;
		}
		
		var options = {
			url : "/addSystemUser.xco",
			data : xco,
			success : addProCallBack
		};
		layer.confirm('是否添加该用户？', {
			title:'提示',
		  	btn: ['是的','不要'] //按钮
		}, function(){
			$.doXcoRequest(options);
		})
	});
	
	function addProCallBack(data){
		if(data.getCode()!=0){
			layer.msg(data.getMessage(), {time: times, icon:no});
		}else{
			layer.msg("用户添加成功", {time: times, icon:ok});
			location.href = '../project/user.jsp';
		}
	}
	
	
	$("#exit").click(function(){
		location.href = '../project/user.jsp';
	})
</script>