<%@ page language="java" import="java.util.*" pageEncoding="utf-8" isELIgnored="false"%>
<!DOCTYPE html>
<%@ include file="../common/comm_css.jsp"%>
<html>

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
 
		<title>用户页面</title>
		<meta name="keywords" content="">
		<meta name="description" content="">
	</head>

	<body class="gray-bg">
		<div class="wrapper wrapper-content animated fadeInRight">
			<div class="row">
				<div class="col-sm-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title" style="height: 50px;padding-top: 10px;">
							<h5 style="margin-top: 8px;">用户页面</h5>
							<div class="ibox-tools">
	                           <button class="btn btn-primary" onclick="location.href='add_user.jsp'">新增用户</button>
                        	</div>
						</div>
						<div class="ibox-content">
							<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline" role="grid">
								<table class="table table-striped table-bordered table-hover dataTables-example">
									<thead>
										<tr>
											<th>用户编号</th>
											<th>登录账户</th>
											<th>用户昵称</th>
											<th>创建时间</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody id="listgroup">
										<!-- 
											<tr>
												<td>#{list[i].user_id}</td>
												<td>#{list[i].user_name}</td>
												<td>#{list[i].real_name}</td>
												<td>@{getTime}</td>
												<td>@{getOpt}</td>
											</tr>
										 -->
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</body>

</html>
<script type="text/javascript">
	XCOTemplate.pretreatment('listgroup');	
	
	getProList();
	function getProList() {
		var xco = new XCO();
		//xco.setIntegerValue("start", _start);
		//xco.setIntegerValue("pageSize", _pageSize);

		var options = {
			url : "/user/getUserList.xco",
			data : xco,
			success : getProListCallBack
		};
		$.doXcoRequest(options);
	}

	function getProListCallBack(data) {
		if (data.getCode() != 0) {
			layer.msg(data.getMessage(), {time: times, icon:no});
		} else {
			var total = 0;
			total = data.getIntegerValue("total");
			num = total;
			var len = 0;
			var list = data.getXCOListValue("list");
			
			var html = "";
			if (list) {
				len = list.length;
			}
			var extendedFunction = {
				getTime : function(){
					return list[i].getStringValue("create_time");
				},
				getOpt : function(){
					return '<a href="edit_user.jsp?user_id='+list[i].getLongValue("user_id")+'">编辑</a>';
				}
			};
			for ( var i = 0; i < len; i++) {
				data.setIntegerValue("i", i);
				html += XCOTemplate.execute('listgroup', data, extendedFunction);
			}
			document.getElementById('listgroup').innerHTML = html;
		}
	}
</script>